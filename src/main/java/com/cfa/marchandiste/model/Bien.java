package com.cfa.marchandiste.model;

public class Bien {
    private String titre;
    private String type;
    private int nbrPiece;
    private int surface;
    private String ville;
    private int cp;
    private int prix;
    private boolean espaceVert = false;

    public Bien(){

    }

    public Bien(String titre, String type, int nbrPiece, int surface, String ville, int cp, int prix, boolean espaceVert) {
        this.titre = titre;
        this.type = type;
        this.nbrPiece = nbrPiece;
        this.surface = surface;
        this.ville = ville;
        this.cp = cp;
        this.prix = prix;
        this.espaceVert = espaceVert;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getNbrPiece() {
        return nbrPiece;
    }

    public void setNbrPiece(int nbrPiece) {
        this.nbrPiece = nbrPiece;
    }

    public int getSurface() {
        return surface;
    }

    public void setSurface(int surface) {
        this.surface = surface;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public int getCp() {
        return cp;
    }

    public void setCp(int cp) {
        this.cp = cp;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    public boolean isEspaceVert() {
        return espaceVert;
    }

    public void setEspaceVert(boolean espaceVert) {
        this.espaceVert = espaceVert;
    }

    @Override
    public String toString() {
        return "Bien{" +
                "titre='" + titre + '\'' +
                ", type='" + type + '\'' +
                ", nbrPiece=" + nbrPiece +
                ", surface=" + surface +
                ", ville='" + ville + '\'' +
                ", cp=" + cp +
                ", prix=" + prix +
                ", espaceVert=" + espaceVert +
                '}';
    }
}
