package com.cfa.marchandiste.model;

public class User {
    private String nom;
    private String prenon;
    private String mdp;
    private String email;
    private String state;

    public User(String nom, String prenon, String mdp, String email, String state) {
        this.nom = nom;
        this.prenon = prenon;
        this.mdp = mdp;
        this.email = email;
        this.state = state;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenon() {
        return prenon;
    }

    public void setPrenon(String prenon) {
        this.prenon = prenon;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "User{" +
                "nom='" + nom + '\'' +
                ", prenon='" + prenon + '\'' +
                ", mdp='" + mdp + '\'' +
                ", email='" + email + '\'' +
                ", state='" + state + '\'' +
                '}';
    }
}
