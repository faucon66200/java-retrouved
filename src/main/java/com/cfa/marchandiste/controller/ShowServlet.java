package com.cfa.marchandiste.controller;

import com.cfa.marchandiste.model.Bien;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@WebServlet(name="ShowServlet",urlPatterns = {"/show"})

public class ShowServlet extends HttpServlet {

    private String csvPath = "/csv/listBien.csv";

    private void doRequest(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
        response.setContentType("text/html;charset=utf-8");
        if(request.getParameter("titre") != null && request.getParameter("init").equals("new")){

        }else{
            request.setAttribute("listeBien",this.getList());
            RequestDispatcher rdisp = request.getRequestDispatcher("ListeBien.jsp");
            rdisp.forward(request,response);
        }
    }

    private List<Bien> getList() throws FileNotFoundException {
        String realPath = getServletContext().getRealPath(this.csvPath);
        FileInputStream csv = new FileInputStream(realPath);
        Scanner sc = new Scanner(csv);
        List<Bien> lb = new ArrayList<Bien>();
        int i = 0;
        while (sc.hasNextLine()){
            String line = sc.nextLine();
            if(i > 0){
                line = line.replaceAll("\"","");
                String[] var = line.split(";");
                Bien bien = new Bien(var[0],
                        var[1],
                        Integer.parseInt(var[2]),
                        Integer.parseInt(var[3]),
                        var[4],
                        Integer.parseInt(var[5]),
                        Integer.parseInt(var[6]),
                        Boolean.parseBoolean(var[7]));
                lb.add(bien);//ajout du bien a la collect
            }
            i++;
        }
        return lb;
    }

    @Override
    protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
        this.doRequest(request,response);
    }
    protected void doGet(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
        this.doRequest(request,response);
    }
}
