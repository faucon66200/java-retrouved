<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport">

    <title>Intitulé de ma page</title>
    <link rel="stylesheet" href="css/main.css">

  </head>
<title>Immo</title>
  <body>
    <!-- Voici notre en‑tête principale utilisée dans toutes les pages
         de notre site web -->
    <header>
      <h1>En-tête</h1>
    </header>

    <nav>
      <ul>
        <li><a href="/">Accueil</a></li>
        <li><a href="login">Vendre un truc</a></li>
        <li><a href="affiche">Projets</a></li>
        <li><a href="#">Contact</a></li>
      </ul>

       <!-- Un formulaire de recherche est une autre façon de naviguer de
            façon non‑linéaire dans un site. -->

       <form>
         <input type="search" name="q" placeholder="Rechercher">
         <input type="submit" value="Lancer !">
       </form>
     </nav>

    <!-- Ici nous mettons le contenu de la page -->
    <main>

      <!-- Il contient un article -->
      <article>
        <h2>En-tête d'article</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Donec a diam lectus. Set sit amet ipsum mauris. Maecenas congue ligula as quam viverra nec consectetur ant hendrerit. Donec et mollis dolor. Praesent et diam eget libero egestas mattis sit amet vitae augue. Nam tincidunt congue enim, ut porta lorem lacinia consectetur.</p>

        <h3>Sous‑section</h3>
        <p>Donec ut librero sed accu vehicula ultricies a non tortor. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aenean ut gravida lorem. Ut turpis felis, pulvinar a semper sed, adipiscing id dolor.</p>
        <p>Pelientesque auctor nisi id magna consequat sagittis. Curabitur dapibus, enim sit amet elit pharetra tincidunt feugiat nist imperdiet. Ut convallis libero in urna ultrices accumsan. Donec sed odio eros.</p>

        <h3>Autre sous‑section</h3>
        <p>Donec viverra mi quis quam pulvinar at malesuada arcu rhoncus. Cum soclis natoque penatibus et manis dis parturient montes, nascetur ridiculus mus. In rutrum accumsan ultricies. Mauris vitae nisi at sem facilisis semper ac in est.</p>
        <p>Vivamus fermentum semper porta. Nunc diam velit, adipscing ut tristique vitae sagittis vel odio. Maecenas convallis ullamcorper ultricied. Curabitur ornare, ligula semper consectetur sagittis, nisi diam iaculis velit, is fringille sem nunc vet mi.</p>
      </article>

      <aside>
        <h2>Listes de biens</h2>
        <p><a href="index.jsp">sdpgilnfdkpo retour</a></p>
        <table>
            <thead>
                <tr>
                    <th colspan="2">titre</th>
                    <th colspan="2">type</th>
                    <th colspan="2">Nbr piece</th>
                    <th colspan="2">Surface</th>
                    <th colspan="2">Ville</th>
                    <th colspan="2">Code posta</th>
                    <th colspan="2">Prix</th>
                    <th colspan="2">Modifier</th>
                    <th colspan="2">Supprimer</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>The table body</td>
                    <td>with two columns</td>
                </tr>
            </tbody>
        </table>
      </aside>

    </main>

    <!-- Et voici notre pied de page utilisé sur toutes les pages du site -->
    <footer>
      <p>©Copyright 2050 par personne. Tous droits reversés.</p>
    </footer>

  </body>
</html>
